/**
 * 配置参考: https://cli.vuejs.org/zh/config/
 */
const path = require('path')
const merge = require('webpack-merge')

const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV)
const Timestamp = new Date().getTime()

function resolve(dir) {
  return path.join(__dirname, dir)
}

const assetsCDN = {
  // webpack build externals
  externals: {
    vue: 'Vue',
    'vue-router': 'VueRouter',
    vuex: 'Vuex',
    axios: 'axios'
  },
  css: [],
  // https://unpkg.com/browse/vue@2.6.10/
  js: [
    '//cdn.jsdelivr.net/npm/vue@2.6.10/dist/vue.min.js',
    '//cdn.jsdelivr.net/npm/vue-router@3.1.3/dist/vue-router.min.js',
    '//cdn.jsdelivr.net/npm/vuex@3.1.1/dist/vuex.min.js',
    '//cdn.jsdelivr.net/npm/axios@0.19.0/dist/axios.min.js'
  ]
}

module.exports = {
  publicPath: IS_PROD ? './' : '/',
  productionSourceMap: false,
  configureWebpack: config => {
    if (IS_PROD) {
      // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
      config.output.filename = `js/[name].js?v=[contenthash:8].${Timestamp}`
      config.output.chunkFilename = `js/[name].js?v=[contenthash:8].${Timestamp}`
    }
    // if prod, add externals
    config.externals = IS_PROD ? assetsCDN.externals : {}
    
  },

  chainWebpack: config => {
    const eslintConfig = config.module.rule('eslint-loader')
    eslintConfig.uses.clear()
    eslintConfig
      .test(/\.(js|vue)$/)
      .use('eslint-loader')
      .loader('eslint-loader')
      .tap(options => {
        return merge(options, {
          fix: true,
          includePaths: [path.join(__dirname, 'src')]
        })
      })
      .end()
    if (IS_PROD) {
      config.plugins.delete('preload')
      config.plugins.delete('prefetch')
      // 压缩代码
      config.optimization.minimize(true)
      // 分割代码
      config.optimization.splitChunks({
        chunks: 'all'
      })

      // assets require on cdn
      if (IS_PROD) {
        config.plugin('html').tap(args => {
          args[0].cdn = assetsCDN
          return args
        })
      }
    }
  },

  devServer: {
    open: true,
    port: 8001,
    proxy: {
      [process.env.VUE_APP_API_PREFIX_URL]: {
        target: process.env.VUE_APP_API_PROXY_URL,
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          [process.env.VUE_APP_API_PREFIX_URL]: ''
        }
      }
    },
    overlay: {
      errors: true,
      warnings: true
    }
  },

  lintOnSave: false
}
