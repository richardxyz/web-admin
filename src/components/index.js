import actionBar from '@/components/actionBar'
import commonPage from '@/components/layout/commonPage'
import Ellipsis from '@/components/Ellipsis'
import detailPage from '@/components/detailPage'
import description from '@/components/detailPage/description'
import areaBox from '@/components/areaBox'
import sticky from '@/components/sticky'
import backToTop from '@/components/backToTop'
export {
  commonPage,
  actionBar,
  Ellipsis,
  detailPage,
  description,
  areaBox,
  sticky,
  backToTop
}
