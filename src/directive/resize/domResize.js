export default {
  bind (el, binding) {
    // eslint-disable-next-line
    let width = '', height = ''
    function get () {
      const style = document.defaultView.getComputedStyle(el)
      if (width !== style.width || height !== style.height) {
        binding.value({ width, height })
      }
      width = style.width
      height = style.height
    }

    el.__vueReize__ = setInterval(get, 20)
  },
  unbind (el) {
    clearInterval(el.__vueReize__)
  }
}
