import { Browser } from '@/utils/tools'
import printJS from 'print-js'
import http from '@/utils/request'
import { Loading, Message } from 'element-ui'

// 打印公共方法
export function printFn (url) {
  let isAjax = false
  if (Browser.isChrome) {
    if (isAjax) {
      return false
    }
    isAjax = true
    const loadStatus = Loading.service({
      lock: true,
      text: 'Loading',
      fullscreen: true,
      // spinner: 'el-icon-loading',
      background: 'rgba(0, 0, 0, 0.6)'
    })
    http({
      method: 'get',
      url: url,
      data: '',
      headers: { 'content-type': 'application/x-www-form-urlencoded' },
      responseType: 'arraybuffer'
    }).then(({ data: res }) => {
      isAjax = false
      loadStatus.close()
      ab2str(res)
      function ab2str (buf) {
        let resJson
        try {
          resJson = JSON.parse(String.fromCharCode.apply(null, new Int8Array(buf)))
          if (resJson.code) {
            return Message({ message: resJson.msg, type: 'error', duration: 5000 })
          }
        } catch (e) {
          let localPdf = new window.Blob([res], { type: 'application/pdf' })
          localPdf = window.URL.createObjectURL(localPdf)
          printJS(localPdf)
        }
      }
    })
  } else {
    window.open('/static/pdf/web/viewer.html?file=' + encodeURIComponent(url))
  }
}
