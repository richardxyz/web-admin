/**
 * 字母转大写
 * @param s
 */
export function toLocaleUpperCase (s) {
  return s.toLocaleUpperCase()
}

/**
 *
 * @description   判断`obj`是否为空
 * @param  {Object} obj
 * @return {Boolean}
 */
export function isEmptyObject (obj) {
  if (!obj || typeof obj !== 'object' || Array.isArray(obj)) { return false }
  return !Object.keys(obj).length
}

/**
 *
 * @description 获取滚动条距顶部的距离
 */
export function getScrollTop () {
  return (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop
}

/**
 *
 * @description 生成指定范围[min, max]的随机数
 * @param  {Number} min
 * @param  {Number} max
 * @return {Number}
 */
export function randomNum (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

/**
 * @description 格式化时间戳
 * @param date
 * @returns {number}
 */
export function timestamp (date) {
  return new Date(date).getTime()
}

/**
 * @description 加法
 * @param number
 * @returns {number}
 */
export function add (arg1, arg2) {
  let r1
  let r2
  let m
  let c
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) {
    r2 = 0
  }
  c = Math.abs(r1 - r2)
  m = Math.pow(10, Math.max(r1, r2))
  if (c > 0) {
    let cm = Math.pow(10, c)
    if (r1 > r2) {
      arg1 = Number(arg1.toString().replace('.', ''))
      arg2 = Number(arg2.toString().replace('.', '')) * cm
    } else {
      arg1 = Number(arg1.toString().replace('.', '')) * cm
      arg2 = Number(arg2.toString().replace('.', ''))
    }
  } else {
    arg1 = Number(arg1.toString().replace('.', ''))
    arg2 = Number(arg2.toString().replace('.', ''))
  }
  return (arg1 + arg2) / m
}

/**
 * @description 减法
 * @param number
 * @returns {number}
 */
export function sub (arg1, arg2) {
  let r1
  let r2
  let m
  let n
  try {
    r1 = arg1.toString().split('.')[1].length
  } catch (e) {
    r1 = 0
  }
  try {
    r2 = arg2.toString().split('.')[1].length
  } catch (e) {
    r2 = 0
  }
  m = Math.pow(10, Math.max(r1, r2))
  n = (r1 >= r2) ? r1 : r2
  return Number(((arg1 * m - arg2 * m) / m).toFixed(n))
}

/**
 * @description 乘法
 * @param number
 * @returns {number}
 */
export function mul (arg1, arg2) {
  let m = 0
  let s1 = arg1.toString()
  let s2 = arg2.toString()
  try {
    m += s1.split('.')[1].length
  } catch (e) {}
  try {
    m += s2.split('.')[1].length
  } catch (e) {}
  return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m)
}

/**
 * @description 除法
 * @param number
 * @returns {number}
 */
export function div (arg1, arg2) {
  let t1 = 0
  let t2 = 0
  let r1
  let r2
  try {
    t1 = arg1.toString().split('.')[1].length
  } catch (e) {}
  try {
    t2 = arg2.toString().split('.')[1].length
  } catch (e) {}
  r1 = Number(arg1.toString().replace('.', ''))
  r2 = Number(arg2.toString().replace('.', ''))
  return (r1 / r2) * Math.pow(10, t2 - t1)
}

/**
 * @description 浏览器检查
 * @param Browser
 * @returns {Browser}
 */
export const Browser = {
  // Firefox 1.0+
  isFirefox: () => {
    return typeof InstallTrigger !== 'undefined'
  },
  // Internet Explorer 6-11
  isIE: () => {
    return navigator.userAgent.indexOf('MSIE') !== -1 || !!document.documentMode
  },
  // Edge 20+
  isEdge: () => {
    return !Browser.isIE() && !!window.StyleMedia
  },
  // Chrome 1+
  isChrome: (context = window) => {
    return !!context.chrome
  },
  // At least Safari 3+: "[object HTMLElementConstructor]"
  isSafari: () => {
    return Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0 ||
        navigator.userAgent.toLowerCase().indexOf('safari') !== -1
  }
}

/**
 * @description 转GMT时间戳
 * @param date
 * @returns {number}
 */
export function toGmtTimestamp (date) {
  let offset = new Date(date).getTimezoneOffset()
  return new Date(date).getTime() + (offset * 60000)
}

export function getBeforeMonth (month) {
  return new Date(getNowDate() - 3600 * 1000 * 24 * month * 30 + 1)
}

export function getNowDate () {
  return new Date(new Date(new Date().toLocaleDateString()).getTime() + 24 * 60 * 60 * 1000 - 1)
}
