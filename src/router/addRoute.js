const addRoute = [
  {
    path: 'exp-form',
    component: () => import(/* webpackChunkName: "exp-form" */ `@/views/modules/listPage/user`),
    name: 'exp-form',
    meta: {
      ...window.SITE_CONFIG['contentTabDefault'],
      menuId: '',
      title: `表单`,
      isTab: false
    }
  }
]
export default addRoute
