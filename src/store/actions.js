import * as types from './mutations-types'

export const tabremoveHandle = ({ commit, state }, router) => {
  commit(types.TAB_REMOVE_HANDLE, router)
}
