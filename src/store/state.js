export default {
  // 导航条, 布局风格, defalut(白色) / colorful(鲜艳)
  navbarLayoutType: 'defalut',
  // 侧边栏, 布局皮肤, default(白色) / dark(黑色)
  sidebarLayoutSkin: 'default',
  // 侧边栏, 折叠状态
  sidebarFold: false,
  // 侧边栏, 菜单
  sidebarMenuList: [],
  sidebarMenuActiveName: '',
  // 内容, 是否需要刷新
  contentIsNeedRefresh: false,
  // 内容, 标签页(默认添加首页)
  contentTabs: [
    {
      ...window.SITE_CONFIG['contentTabDefault'],
      name: 'home',
      title: 'home'
    }
  ],
  contentTabsActiveName: 'home'
}
