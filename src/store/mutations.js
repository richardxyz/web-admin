import * as types from './mutations-types'

const mutations = {
  [types.TAB_REMOVE_HANDLE] (state, router) {
    tabRemoveHandle(state, state.contentTabsActiveName, router)
  }
}

function tabRemoveHandle (state, tabName, router) {
  if (tabName === 'home') {
    return false
  }
  state.contentTabs = state.contentTabs.filter(item => item.name !== tabName)
  if (state.contentTabs.length <= 0) {
    state.sidebarMenuActiveName = state.contentTabsActiveName = 'home'
    return false
  }
  // 当前选中tab被删除
  const tab = state.contentTabs[state.contentTabs.length - 1]
  if (tabName === state.contentTabsActiveName) {
    router.push({
      name: tab.name,
      params: tab.params,
      query: tab.query
    })
  }
}

export default mutations
