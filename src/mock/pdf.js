const Mock = require('mockjs')
const info = {
  pdf: config => {
    return '/static/pdf/web/demo.pdf'
  }
}

Mock.mock(/\/bd\/pdf/, 'get', info.pdf)
