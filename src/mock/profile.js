const Mock = require('mockjs')
const profile = {
  base: config => {
    return {
      code: 0,
      msg: 'success',
      data: {
        id: '1186206854485688321',
        ecPlatform: 'EBAY',
        ecProductBarcode: 'ABC',
        productCode: 'ZL_2019002',
        productBarcode: '2019002',
        productName: 'ZL',
        customerId: '乐扣',
        customerName: '乐扣',
        companyId: '1131841415962198018',
        creator: '1137998048794841090',
        createDate: '2019-10-21 09:05:27',
        updater: '1137998048794841090',
        updateDate: '2019-10-21 09:05:27',
        status: 0,
        version: 0,
        creatorName: '乐扣'
      }
    }
  },
  advanced: config => {
    return {
      code: 0,
      msg: 'success',
      data: {
        bdRuleChannelConditionInstDTOS: [
          {
            id: '1164385377809674241',
            ruleChannelInstId: '1164385377801285633',
            logisticsChannelId: '京华达测试渠道',
            addReceive: 0,
            remoteDelivery: 0,
            priority: 1,
            creator: '1161894101801517057',
            createDate: '2019-08-22 03:54:42'
          }
        ],
        id: '1164385377801285633',
        name: '1153',
        warehouseId: '1131868606116777985',
        startWeighD: 20.0,
        endWeightD: 30.0,
        strategy: 1,
        remark: '',
        customerId: '1137998048794841090',
        companyId: '1131841415962198018',
        creator: '1161894101801517057',
        createDate: '2019-08-22 03:54:42',
        updater: '1161894101801517057',
        updateDate: '2019-08-27 09:33:24',
        status: 10,
        delFlag: 0,
        version: 0
      }
    }
  }
}

Mock.mock(/\/profile\/base/, 'get', profile.base)
Mock.mock(/\/profile\/advanced/, 'get', profile.advanced)
