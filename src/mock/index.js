
if (process.env.NODE_ENV.includes('dev')) {
  const Mock = require('mockjs')
  require('./login')
  require('./nav')
  require('./info')
  require('./profile')
  require('./pdf')
  Mock.setup({
    timeout: 800 // setter delay time
  })
}
