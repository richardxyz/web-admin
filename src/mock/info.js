const Mock = require('mockjs')
const info = {
  product: config => {
    return {
      code: 0,
      msg: 'success',
      data: {
        total: 54,
        list: [
          {
            id: '1232864342273384449',
            code: 'HH000002',
            barcode: 'B888PPPEEE3',
            name: 'testProduct2',
            enName: 'testProduct2',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 0.5,
            lengthD: 16.0,
            widthD: 17.0,
            heightD: 18.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '商品导入test',
            ifFragile: 0,
            specType: 'test',
            brand: 'NIKE',
            category: 'test',
            categoryCode: '12990000003',
            declaredPriceD: 0.25,
            categoryName1: '母婴用品',
            categoryName2: '婴儿用品',
            categoryName3: '婴儿床',
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2020-02-27 03:05:39',
            updater: '1137998048794841090',
            updateDate: '2020-02-27 03:05:39',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 0.5,
            wsWeightD: null,
            wsWeightUnit: null,
            kiloWsWeightD: null,
            wsLengthD: null,
            wsWidthD: null,
            wsHeightD: null,
            wsLengthUnit: null,
            cmLengthD: 16.0,
            cmHeightD: 18.0,
            cmWidthD: 17.0
          },
          {
            id: '1232864342181109761',
            code: 'HH000001',
            barcode: 'B666PPPTTT3',
            name: 'testProduct1',
            enName: 'testProduct1',
            weightUnit: 'g',
            lengthUnit: 'cm',
            weightD: 1.0,
            lengthD: 15.0,
            widthD: 16.0,
            heightD: 19.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '商品导入',
            ifFragile: 0,
            specType: 'test',
            brand: 'NIKE',
            category: 'test',
            categoryCode: '12990000003',
            declaredPriceD: 0.25,
            categoryName1: '母婴用品',
            categoryName2: '婴儿用品',
            categoryName3: '婴儿床',
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2020-02-27 03:05:39',
            updater: '1137998048794841090',
            updateDate: '2020-02-27 03:05:39',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 0.001,
            wsWeightD: null,
            wsWeightUnit: null,
            kiloWsWeightD: null,
            wsLengthD: null,
            wsWidthD: null,
            wsHeightD: null,
            wsLengthUnit: null,
            cmLengthD: 15.0,
            cmHeightD: 19.0,
            cmWidthD: 16.0
          },
          {
            id: '1207874209435463682',
            code: '1111',
            barcode: '2222',
            name: '3333',
            enName: '44444',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 1.0,
            lengthD: 1.0,
            widthD: 1.0,
            heightD: 1.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: '',
            category: '',
            categoryCode: '',
            declaredPriceD: 0.0,
            categoryName1: null,
            categoryName2: null,
            categoryName3: null,
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-12-20 04:03:47',
            updater: '1137998048794841090',
            updateDate: '2020-02-25 03:27:56',
            status: 0,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 1.0,
            wsWeightD: null,
            wsWeightUnit: null,
            kiloWsWeightD: null,
            wsLengthD: null,
            wsWidthD: null,
            wsHeightD: null,
            wsLengthUnit: null,
            cmLengthD: 1.0,
            cmHeightD: 1.0,
            cmWidthD: 1.0
          },
          {
            id: '1187298484554915841',
            code: 'TEST1024',
            barcode: 'TEST1024',
            name: 'TEST1024',
            enName: 'TEST1024',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 1.0,
            lengthD: 1.0,
            widthD: 1.0,
            heightD: 1.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: '',
            category: '',
            categoryCode: '',
            declaredPriceD: 0.0,
            categoryName1: null,
            categoryName2: null,
            categoryName3: null,
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-24 09:23:12',
            updater: '1137998048794841090',
            updateDate: '2019-10-24 09:23:12',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 1.0,
            wsWeightD: 0.5,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 1.0,
            wsWidthD: 1.0,
            wsHeightD: 1.0,
            wsLengthUnit: 'cm',
            cmLengthD: 1.0,
            cmHeightD: 1.0,
            cmWidthD: 1.0
          },
          {
            id: '1187296333690658817',
            code: 'ZL_SKU_001',
            barcode: 'ZL_20191024001',
            name: '尿不湿',
            enName: 'cup',
            weightUnit: 'g',
            lengthUnit: 'cm',
            weightD: 2.5,
            lengthD: 5.0,
            widthD: 5.0,
            heightD: 10.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: 'mike',
            category: '',
            categoryCode: '27000000062',
            declaredPriceD: 0.0,
            categoryName1: '母婴用品',
            categoryName2: '婴儿用品',
            categoryName3: '纸尿裤',
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-24 09:14:40',
            updater: '1137998048794841090',
            updateDate: '2019-10-24 09:14:40',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 0.0025,
            wsWeightD: 0.5,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 2.0,
            wsWidthD: 2.0,
            wsHeightD: 2.0,
            wsLengthUnit: 'cm',
            cmLengthD: 5.0,
            cmHeightD: 10.0,
            cmWidthD: 5.0
          },
          {
            id: '1187201499327918081',
            code: 'ZWJ_001',
            barcode: 'ZWJ_001_001',
            name: '周伟军测试',
            enName: 'zhouwj',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 0.1,
            lengthD: 15.0,
            widthD: 15.0,
            heightD: 15.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: '',
            category: '',
            categoryCode: '',
            declaredPriceD: 0.0,
            categoryName1: null,
            categoryName2: null,
            categoryName3: null,
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-24 02:57:49',
            updater: '1137998048794841090',
            updateDate: '2019-10-24 02:57:49',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 0.1,
            wsWeightD: 0.1,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 15.0,
            wsWidthD: 15.0,
            wsHeightD: 15.0,
            wsLengthUnit: 'cm',
            cmLengthD: 15.0,
            cmHeightD: 15.0,
            cmWidthD: 15.0
          },
          {
            id: '1186572341719609346',
            code: 'XX-KO',
            barcode: 'XX-KO',
            name: 'XX-KO',
            enName: 'XX-KO',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 1.0,
            lengthD: 42.0,
            widthD: 15.0,
            heightD: 20.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: 'XX-KO',
            category: '',
            categoryCode: '',
            declaredPriceD: 0.0,
            categoryName1: null,
            categoryName2: null,
            categoryName3: null,
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-22 09:17:46',
            updater: '1137998048794841090',
            updateDate: '2019-10-22 09:17:46',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 1.0,
            wsWeightD: 1.5,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 20.0,
            wsWidthD: 20.0,
            wsHeightD: 20.0,
            wsLengthUnit: 'cm',
            cmLengthD: 42.0,
            cmHeightD: 20.0,
            cmWidthD: 15.0
          },
          {
            id: '1186505558526599170',
            code: 'AIRJORDAN',
            barcode: 'AIRJORDAN',
            name: 'AIRJORDAN',
            enName: 'AIRJORDAN',
            weightUnit: 'kg',
            lengthUnit: 'cm',
            weightD: 1.0,
            lengthD: 43.0,
            widthD: 25.0,
            heightD: 20.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 1,
            specType: '',
            brand: 'AIRJORDAN',
            category: '',
            categoryCode: '06020100002',
            declaredPriceD: 0.0,
            categoryName1: '箱包和鞋靴',
            categoryName2: '鞋靴',
            categoryName3: '皮鞋',
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-22 04:52:24',
            updater: '1137998048794841090',
            updateDate: '2020-02-26 08:54:24',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 1.0,
            wsWeightD: 1.5,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 43.0,
            wsWidthD: 48.0,
            wsHeightD: 22.0,
            wsLengthUnit: 'cm',
            cmLengthD: 43.0,
            cmHeightD: 20.0,
            cmWidthD: 25.0
          },
          {
            id: '1186188843557076994',
            code: 'ZL_2019002',
            barcode: '2019002',
            name: 'ZL',
            enName: 'ZL',
            weightUnit: 'g',
            lengthUnit: 'mm',
            weightD: 0.12,
            lengthD: 10.0,
            widthD: 10.0,
            heightD: 10.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: '',
            category: '',
            categoryCode: '27000000062',
            declaredPriceD: 0.0,
            categoryName1: '母婴用品',
            categoryName2: '婴儿用品',
            categoryName3: '纸尿裤',
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-21 07:53:53',
            updater: '1137998048794841090',
            updateDate: '2019-10-21 07:53:53',
            status: 1,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 1.2e-4,
            wsWeightD: 1.0,
            wsWeightUnit: 'kg',
            kiloWsWeightD: null,
            wsLengthD: 1.0,
            wsWidthD: 1.0,
            wsHeightD: 1.0,
            wsLengthUnit: 'cm',
            cmLengthD: 1.0,
            cmHeightD: 1.0,
            cmWidthD: 1.0
          },
          {
            id: '1186180232357363714',
            code: 'AAA',
            barcode: '111',
            name: '1122',
            enName: 'AA111',
            weightUnit: 'g',
            lengthUnit: 'cm',
            weightD: 111.0,
            lengthD: 1.0,
            widthD: 1.0,
            heightD: 1.0,
            returnStrategy: 10,
            pickStrategy: 0,
            packType: 1,
            memo: '',
            ifFragile: 0,
            specType: '',
            brand: 'NIKE',
            category: '',
            categoryCode: '',
            declaredPriceD: 0.0,
            categoryName1: null,
            categoryName2: null,
            categoryName3: null,
            customerId: '1137998048794841090',
            customerName: '乐扣',
            companyId: '1131841415962198018',
            creator: '1137998048794841090',
            createDate: '2019-10-21 07:19:40',
            updater: '1137998048794841090',
            updateDate: '2019-10-21 07:19:40',
            status: 0,
            delFlag: 0,
            version: 0,
            bdProductMeasureDTO: null,
            kiloWeightD: 0.111,
            wsWeightD: null,
            wsWeightUnit: null,
            kiloWsWeightD: null,
            wsLengthD: null,
            wsWidthD: null,
            wsHeightD: null,
            wsLengthUnit: null,
            cmLengthD: 1.0,
            cmHeightD: 1.0,
            cmWidthD: 1.0
          }
        ]
      }
    }
  },
  log: config => {
    return {
      code: 0,
      msg: 'success',
      data: {
        total: 5693,
        list: [
          {
            id: '1242363385035624450',
            operation: 0,
            status: 1,
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36',
            ip: '119.123.72.165,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 16:11:25'
          },
          {
            id: '1242363343059030018',
            operation: 0,
            status: 0,
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '14.107.156.36,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 16:11:16'
          },
          {
            id: '1242360742330503170',
            operation: 0,
            status: 0,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '119.112.251.220,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 16:00:55'
          },
          {
            id: '1242359567782776834',
            operation: 0,
            status: 1,
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '125.38.34.123,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 15:56:10'
          },
          {
            id: '1242356841107689473',
            operation: 0,
            status: 0,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '112.17.176.64,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 15:45:20'
          },
          {
            id: '1242352520567721985',
            operation: 0,
            status: 1,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
            ip: '120.239.197.208,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 15:28:11'
          },
          {
            id: '1242352143017447425',
            operation: 0,
            status: 1,
            userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '39.82.33.152,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 15:26:44'
          },
          {
            id: '1242350297053925377',
            operation: 0,
            status: 2,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36',
            ip: '183.14.132.82,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 15:19:25'
          },
          {
            id: '1242341614047727617',
            operation: 0,
            status: 2,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
            ip: '218.13.15.114,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 14:44:54'
          },
          {
            id: '1242339390848503809',
            operation: 0,
            status: 1,
            userAgent: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.120 Safari/537.36',
            ip: '183.130.35.206,192.168.32.1',
            creatorName: 'admin',
            createDate: '2020-03-24 14:35:57'
          }
        ]
      }
    }
  }
}

Mock.mock(/\/bd\/product\/page/, 'get', info.product)
Mock.mock(/\/bd\/log/, 'get', info.log)
