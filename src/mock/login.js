const Mock = require('mockjs')
function param2Obj(url) {
  const search = url.split('?')[1]
  if (!search) {
    return {}
  }
  return JSON.parse(
    '{"' +
      decodeURIComponent(search)
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"')
        .replace(/\+/g, ' ') +
      '"}'
  )
}

const userMap = {
  admin: {
    roles: ['admin'],
    token: 'admin',
    introduction: '我是超级管理员',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Super Admin'
  },
  editor: {
    roles: ['editor'],
    token: 'editor',
    introduction: '我是编辑',
    avatar: 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
    name: 'Normal Editor'
  }
}

const loginAPI = {
  loginByUsername: config => {
    const {username} = JSON.parse(config.body)
    return userMap[username]
  },
  getUserInfo: config => {
    const {token} = param2Obj(config.url)
    if (userMap[token]) {
      return userMap[token]
    } else {
      return false
    }
  },
  info: config => {
    return {
      code: 0,
      msg: 'success',
      data: {
        id: '1067246875800000001',
        username: 'admin',
        realName: '超级管理员',
        headUrl: null,
        gender: 1,
        email: 'root@baidu.com',
        mobile: '13512345Ò678',
        deptId: null,
        superAdmin: 1,
        status: 1,
        remark: null,
        createDate: null,
        roleIdList: null,
        deptName: null
      }
    }
  },
  permissions: config => {
    return {
      code: 0,
      msg: 'success',
      data: [
        'sys:params:export',
        'sys:menu:update',
        'sys:menu:delete',
        'sys:params:update',
        'sys:dept:update',
        'sys:dict:delete',
        'sys:user:export',
        'sys:dept:delete',
        'sys:params:delete',
        'sys:role:view',
        'sys:dict:view',
        'sys:schedule:resume',
        'sys:dept:view',
        'sys:user:delete',
        'sys:user:update',
        'sys:region:update',
        'sys:region:view',
        'sys:menu:view',
        'sys:schedule:update',
        'sys:schedule:save',
        'sys:params:save',
        'sys:user:view',
        'sys:menu:save',
        'sys:role:save',
        'sys:region:delete',
        'sys:schedule:log',
        'sys:schedule:delete',
        'sys:role:update',
        '0',
        'sys:schedule:view',
        'sys:region:save',
        'sys:schedule:run',
        'sys:notice:all',
        'sys:params:view',
        'sys:role:delete',
        'sys:user:save',
        'sys:dept:save',
        'sys:dict:update',
        'sys:schedule:pause',
        'sys:dict:save'
      ]
    }
  },
  logout: () => 'success'
}

Mock.mock(/\/auth\/login/, 'post', loginAPI.loginByUsername)

Mock.mock(/\/sys\/user\/info/, 'get', loginAPI.info)

Mock.mock(/\/sys\/menu\/permissions/, 'get', loginAPI.permissions)
