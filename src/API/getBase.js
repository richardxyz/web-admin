/*
* 获取基础数据
* url 接口地址
* param 接口需要用到的参数
*
* 使用在组件键引入方法
*
* */
import http from '@/utils/request'
import Message from 'element-ui'

export default (url, param = {}) => {
  return new Promise((resolve, reject) => {
    http.get(url, { params: param }).then(({ data: res }) => {
      if (res.code !== 0) {
        reject(res)
        Message.error(res.msg)
        return false
      }
      resolve(res.data)
    }).catch((res) => {
      Message.error(res)
      reject(res)
    })
  })
}
