import domResize from '@/directive/resize'
export default {
  directives: { domResize },
  data() {
    return {
      searchBoxShow: false,
      tableHeight: 0,
      panelHeight: '0px'
    }
  },
  created() {
    window.onresize = () => {
      this.redraw()
      this.rePanel()
    }
  },
  beforeDestroy() {
    window.onresize = null
  },
  methods: {
    redraw() {
      this.$nextTick(() => {
        if (this.$refs.tableElm) {
          this.tableHeight = this.$refs.tableElm.offsetHeight - 52
        }
      })
    },
    rePanel() {
      this.$nextTick(() => {
        if (this.$refs.searchBox) {
          this.panelHeight = `calc(calc(100vh - 64px - 38px - 30px - ${this.$refs.searchBox.offsetHeight}px))`
        }
      })
    },
    // 展示与隐藏搜索条件
    searchBoxShowFn() {
      this.searchBoxShow = !this.searchBoxShow
    }
  }
}
