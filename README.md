# web-admin

## Project setup
```
yarn install
```

### bug cannot read property 'range' of null
```
yarn add @vue/cli-plugin-eslint@3.0.4 -D
```

### Compiles and hot-reloads for development
```
yarn serve
```


### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```
