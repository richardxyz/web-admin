const plugins = []
if (['production', 'prod'].includes(process.env.NODE_ENV)) {
  plugins.push('transform-remove-console')
}else {
  // plugins.push('dynamic-import-node')
}

module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins
}
